
//Funciones aleatorias

function Generar(){

    //Numero aleatorio entero = edad
    function edad() {
        return Math.floor((Math.random() * (99 - 18 + 1)) + 18);
    }
    //Numero aleatorio real = altura y peso
    function altura() {
        return Math.random() * (2.5 - 1.5 + 1) + 1.5;
    }

    function peso() {
        return Math.random() * (130 - 20 + 1) + 20;
    }

    var edad1 = edad();
    var altura1 = altura();
    var peso1 = peso();

    let edadp = edad1.toFixed(2);
    let alturap = altura1.toFixed(2);
    let pesop = peso1.toFixed(2);

    //Asignar valores a los inputs
    document.getElementById('edad').value=edadp;
    document.getElementById('peso').value=pesop;
    document.getElementById('altura').value=alturap;

}


function Calcular(){

    var alturac = document.getElementById('altura').value;
    var pesoc = document.getElementById('peso').value;

    var indice = (pesoc / Math.pow(alturac, 2));
    var indicep = indice.toFixed(2);

    if (indice < 21){
        document.getElementById('IMC').value=indicep;
        document.getElementById('nivel').value = "Bajo peso";
    }else if (indice >= 21 && indice < 25){
        document.getElementById('IMC').value=indicep;
        document.getElementById('nivel').value = "Peso saludable";
    }else if (indice >= 25 && indice < 30){
        document.getElementById('IMC').value=indicep;
        document.getElementById('nivel').value = "Sobrepeso";
    }else{
        document.getElementById('IMC').value=indicep;
        document.getElementById('nivel').value = "obesidad";
    }		  


}

    var acumulado = 0;
function Registro(){

    let acumular = document.getElementById('IMC').value;
    let parrafo = document.getElementById('tabla');
    let promedio = document.getElementById('promedio');
    let indice1 =document.getElementById('IMC').value;
    let nivel = document.getElementById('nivel').value;
    let edad2 = document.getElementById('edad').value;
    let altura2 = document.getElementById('altura').value;
    let peso2 = document.getElementById('peso').value;
    let parrafo2;
    
    parrafo2 = " " + edad2 + "__|__" + altura2 + "__|__" + peso2 + "__|__" + indice1 + "__|__"
    + nivel + "<br>";
    parrafo.innerHTML= parrafo.innerHTML + parrafo2;

    acumulado = parseFloat(acumular) + acumulado ;
    promedio.innerText=acumulado;
}

function Borrar(){
    let parrafo=document.getElementById('tabla');
    let promedio= document.getElementById('promedio');
    promedio.innerHTML="";
    parrafo.innerHTML="<br>";
}